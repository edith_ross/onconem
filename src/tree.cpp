#include "tree.h"

#include <Rcpp.h>
#include <limits>
#include <sstream>

class TreeFinderWrapper {
public:
  /**
   * Creates a new tree finder object with the given scoring data
   * (which also define the graph size).
   *
   * Each measurement is encoded as 0,..,numOutcomes and the
   * likelihood lists must be long enough.
   *
   * \param trueScore List of likelihoods when predicting a mutation
   * \param falseScore List of likelihoods when not predicting a mutation
   * \param obsData m x n Matrix defining the measured outcome.
   *                Here n is the number of non-root nodes and
   *                defines the size.
   *                The (i,j) entry gives the measurement for the
   *                i^th SNV at the j^th non-root node.
   */
  TreeFinderWrapper(const Rcpp::NumericVector & trueScore,
		    const Rcpp::NumericVector & falseScore,
		    const Rcpp::IntegerMatrix & obsData)

  {
    // Get number of non-root nodes
    unsigned n = obsData.ncol();
    // Get number of observations/SNVs
    unsigned m = obsData.nrow();
    // Get numOutcomes
    if (trueScore.size() != falseScore.size()) {
      throw std::length_error("Likelihoods for mutated/unmutated do not have matching size");
    }
    unsigned numOutcomes = trueScore.size();
    // Create C array for observation data and validate outcome
    outcomeIndex_t * obsDataC = new outcomeIndex_t[n*m];
    for (unsigned i=0; i < m; ++i) {
      for (unsigned j=0; j < n; ++j) {
	if ((unsigned) obsData(i,j) >= numOutcomes) {
	  std::ostringstream estream;
	  estream << "Invalid outcome: Number of given scores is "
		    << numOutcomes
		    << " but entry (" << (i+1) << "," << (j+1)
		    << ") is " << obsData(i,j);
	  throw std::invalid_argument(estream.str());
	}
	obsDataC[i*n + j] = obsData(i,j);
      }
    }
    // Create a C array for the outcomes scores
    double * trueScoreC = new double[numOutcomes];
    double * falseScoreC = new double[numOutcomes];
    for (unsigned i=0; i < numOutcomes; ++i) {
      trueScoreC[i] = trueScore(i);
      falseScoreC[i] = falseScore(i);
    }
    // Create wrapped C++ object
    tf = new TreeFinder(m, n, numOutcomes, trueScoreC, falseScoreC, obsDataC);
    // Clean up
    delete [] obsDataC;
    delete [] trueScoreC;
    delete [] falseScoreC;
  }
  // TODO Copy constructor
  /**
   * Frees the memory with deconstruction
   */
  ~TreeFinderWrapper() {
    delete tf;
  }

  /**
   * Wraps the function for adding a tree
   *
   * Returns false if the tree could not be added because it was
   * already considered or because it is invalid.
   *
   * \param tree List of parents of the non-root nodes
   * \return True if the tree was added
   */
  bool addTree(Rcpp::IntegerVector tree) {
    // Test size
    if (tree.size() != tf->getN()) {
      return false;
    }
    // Create c array
    nodeIndex_t * treeC = new nodeIndex_t[tf->getN()];
    for (unsigned i=0; i < tf->getN(); ++i) {
      treeC[i] = tree(i);
    }
    // Add tree (which verifies the tree as well)
    bool result = tf->addTree(treeC);
    delete [] treeC;
    return result;
    }

  /**
   * Wraps the find function that aborts after N steps. 
   */
  std::string find(unsigned steps, bool swap) {
    return tf->find(steps,swap);
  }

  /**
   * Wraps alternative find function that aborts if highest scoring tree hasn't changed for delta steps. 
   */
  std::string find2(unsigned delta, bool swap)  {
    return tf->find2(delta,swap);
  }

  /** 
   * Wraps the getCurrCounter function
   */
  unsigned getCurrCounter() {
    return tf->getCurrCounter();
  }
  
  /** 
   * Wraps the getBestCounter function
   */
  unsigned getBestCounter() {
    return tf->getBestCounter();
  }

  /**
   * Wraps the scoring
   * \param tree List of parents of the non-root nodes
   * \return Score, Return +inf when we encounter an error
   */
  double scoreTree(Rcpp::IntegerVector tree) {
    // Test size
    if (tree.size() != tf->getN()) {
      std::cerr << "Tree must be given as vector of length n!\n";
      return std::numeric_limits<double>::infinity();
    }
    // Create c array
    nodeIndex_t * treeC = new nodeIndex_t[tf->getN()];
    for (unsigned i=0; i < tf->getN(); ++i) {
      treeC[i] = tree(i);
    }
    double result = tf->scoreTree(treeC);
    delete [] treeC;
    return result;
  }

  double scoreCluster(Rcpp::IntegerVector tree,
		      Rcpp::IntegerVector clustAllocation) {
    if (clustAllocation.size() != tf->getN()) {
      std::cerr << "clustAllocation must be given as vector of length n!\n";
      return std::numeric_limits<double>::infinity();
    }
    // Create c arrays
    nodeIndex_t * treeC = new nodeIndex_t[tree.size()];
    for (unsigned i=0; (int) i < tree.size(); ++i) {
      treeC[i] = tree(i);
    }
    nodeIndex_t * clustAllocationC = new nodeIndex_t[tf->getN()];
    for (unsigned i=0; i < tf->getN(); ++i) {
      clustAllocationC[i] = clustAllocation(i);
    }
    double result = tf->scoreCluster(tree.size(),treeC,clustAllocationC);
    delete [] treeC;
    delete [] clustAllocationC;
    return result;
  }

  /**
  * Wraps the posterior probability calculation
  * \param tree List of parents of the non-root nodes
  * \return Score, Return +inf when we encounter an error
  */		       
  Rcpp::NumericMatrix getPostProb(Rcpp::IntegerVector tree) {
    // Test size
    if (tree.size() != tf->getN()) {
      return std::numeric_limits<double>::infinity();
    }
    // Create c array for tree
    nodeIndex_t * treeC = new nodeIndex_t[tf->getN()];
    for (unsigned i=0; i < tf->getN(); ++i) {
      treeC[i] = tree(i);
    }
    // Create array to store postProb
    double * postProb = new double[(tf->getN()*tf->getM())];
    tf->getPostProb(treeC,postProb);
    // convert C array to Rcpp::NumericMatrix
    Rcpp::NumericMatrix postProbMat(tf->getM(), tf->getN());
    for (unsigned i=0; i < tf->getM(); ++i) { 
      for (unsigned j=0; j < tf->getN(); ++j) {
	postProbMat(i,j) = postProb[i*tf->getN()+j] ;
      }
    }
    
    delete [] treeC;
    delete [] postProb;
    return postProbMat;
  }

  /**
   * Returns the number of considered trees
   * \return n : number of considered trees
   */
  unsigned getNConsideredTrees() const {
    return tf->getScoredTreeList().size();
  }

  /**
   * Returns a list of scores of the considered trees. Sorted by score.
   * \param n : maximal length returned
   * \return list : List of scores
   */
  Rcpp::NumericVector getScoreList(unsigned nTree) const {
    Rcpp::NumericVector scores(std::min(nTree, this->getNConsideredTrees()));
    unsigned i = 0;
    ScoredTreeSet::const_iterator it = tf->getScoredTreeList().begin();
    for (; i < nTree && it != tf->getScoredTreeList().end(); ++i, ++it) {
      scores(i) = it->first;
    }
    return scores;
  }

  /**
   * Returns a list of the considered trees. Sorted by score.
   * Each row represents one graph
   * \param n : maximal length returned
   * \return treeMatrix : Matrix of the considered graphs
   */
  Rcpp::IntegerMatrix getTreeList(unsigned nTree) const {
    Rcpp::IntegerMatrix trees(std::min(nTree, this->getNConsideredTrees()),
			      tf->getN());
    unsigned i = 0;
    ScoredTreeSet::const_iterator it = tf->getScoredTreeList().begin();
    for (; i < nTree && it != tf->getScoredTreeList().end(); ++i, ++it) {
      const nodeIndex_t * tree = it->second;
      for (unsigned j=0; j < tf->getN(); ++j) {
	trees(i,j) = tree[j];
      }
    }
    return trees;
  }

private:
  /**
   * Wrapped C++ object
   */
  TreeFinder * tf;
};

RCPP_MODULE(treeFinder_module) {
  Rcpp::class_<TreeFinderWrapper>( "TreeFinder" )
    
    .constructor<Rcpp::NumericVector,Rcpp::NumericVector,Rcpp::IntegerMatrix>
    ("Constructs a tree finder object with the given scoring data")
    
    .method("addTree", &TreeFinderWrapper::addTree,
	    "Adds a tree to the search lists.")
    .method("find", &TreeFinderWrapper::find,
	    "Find best scoring tree by local permutations and stop after certain number of steps.")
    .method("find2", &TreeFinderWrapper::find2,
	    "Find best scoring tree by local permutations and stop if solution has not changed for at delta steps.")
    .method("getCurrCounter", &TreeFinderWrapper::getCurrCounter,
	    "Returns the overall number of search steps ")
    .method("getBestCounter", &TreeFinderWrapper::getBestCounter,
	    "Returns the step in which the best scoring tree was found.")
    .method("scoreTree", &TreeFinderWrapper::scoreTree,
	    "Scores a tree")
    .method("scoreCluster", &TreeFinderWrapper::scoreCluster,
	    "Scores a clustered tree")
    .method("getPostProb", &TreeFinderWrapper::getPostProb,
	    "Calculates posterior probability for a given tree")
    .method("getNConsideredTrees", &TreeFinderWrapper::getNConsideredTrees,
	    "Returns the number of considered trees")
    .method("getScoreList", &TreeFinderWrapper::getScoreList,
	    "Returns the list of scores")
    .method("getTreeList", &TreeFinderWrapper::getTreeList,
	    "Returns the list of trees")
    ;
}
