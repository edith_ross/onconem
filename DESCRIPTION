Package: oncoNEM
Type: Package
Title: Inferring trees of clonal evolution from single-cell SNVs
Version: 1.0
Date: 2015-09-01
Author: Edith M Ross and Florian Markowetz
Maintainer: Edith M Ross <Edith.Ross@cruk.cam.ac.uk>
Description: This package provides classes and methods for oncoNEM inference. OncoNEM is an automated method for reconstructing clonal lineage trees from SNVs of multiple single tumour cells that exploits the nested structure of mutation patterns of related cells. While accounting for the high error rates of single-cell sequencing, oncoNEM identifies subpopulations and unobserved ancestral subpopulations including their genotypes and evolutionary relationships.
License: GPL-3
LazyData: true
Depends: Rcpp (>= 0.11.0)
Imports: methods, igraph, ggm
LinkingTo: Rcpp
Suggests: ggplot2, xtable, reshape2, bitphylogenyR, foreach, doParallel
RcppModules: treeFinder_module
NeedsCompilation: yes