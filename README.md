# Overview

OncoNEM is an R package accompanying the following publication:

**OncoNEM: Inferring tumour evolution from single-cell sequencing data**   
E.M. Ross and F. Markowetz   
Genome Biology 2016, 17:69   
PMID:27083415 | doi:10.1186/s13059-016-0929-9

# License

OncoNEM is licensed under the GPL v3, see the License.txt file for details.

# Installation
OncoNEM can be installed directly from Bitbucket using the devtools R package by executing the following code in an R terminal:
```
library(devtools)
install_bitbucket("edith_ross/oncoNEM")
```

# Usage
Please refer to the vignette for a toy example.